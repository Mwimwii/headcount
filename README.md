# Headcount system

## Installation
```bash
git clone https://gitlab.com/Mwimwii/headcount
```

## Configuration
### Windows
```bash
set FLASK_ENV=development
set FLASK_APP=headcount
```
### Linux
```bash
export FLASK_ENV=development
export FLASK_APP=headcount
```
## Run the app
```bash
flask run
```
