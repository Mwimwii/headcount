from functools import wraps


from flask import (
    Blueprint,  g, redirect, session, url_for, flash
)
from flask.globals import request
from flask.templating import render_template
from headcount.models import Head, db
import random, string
from headcount.forms import LoginForm

bp = Blueprint('auth', __name__, url_prefix='/auth')

# Allow the user to be available on other views
@bp.before_app_request
def load_logged_in_user():
    employee_id = session.get('employee_id')
    if employee_id is None:
        g.user = None
    else:
        g.user = db.session.query(Head).filter(Head.id == employee_id).first()

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user is None:
            return redirect(url_for('auth.login_user'))
        return f(*args, **kwargs)
    return decorated_function

def generate_link():
    return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))

@bp.route("/login/<employee_id>_<credentials>8g9")
def login(employee_id, credentials):
    employee = db.session.query(Head).filter(Head.id == employee_id).first()
    if employee is not None:
        if credentials == employee.link:
            if employee.confirmed ==  0:
                employee.confirmed = 1
                db.session.commit()
            session.clear()
            session['employee_id'] = employee.id
            # Successful login
            return redirect(url_for("admin.index"))
        else:
            return "Invalid or Expired Credentials"
    return "Invalid link, please refer to the admin@szi.gov.zm or check your email for the confirmation link"

@bp.route("/", methods=["GET", "POST"])
def login_user():
    form = LoginForm()
    if form.validate_on_submit():
        head = db.session.query(Head).filter_by(email_address=form.email.data).first()
        if head is not None:
        # Send the email
            return redirect(url_for("admin.send_one", user=head.id))
        else:
            error = "User does not exist please register"
            flash(error)
    return render_template("login.html", form=form)
