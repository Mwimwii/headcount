from enum import unique
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql.schema import ForeignKey
def generate_link():
    return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(16))

import string, random
db = SQLAlchemy()

def new_staff_return(employee, subordinate, confirmed, office ):
    subordinate = StaffReturn(
                            name=employee.get("name")[1:-1],
                            employee_number=employee.get("employee_number")[1:-1],
                            current_position=employee.get("current_position")[1:-1],
                            email_address=employee.get("email_address")[1:-1],
                            months_acting=employee.get("months_acting")[1:-1],
                            status=employee.get("status")[1:-1],
                            confirmed=confirmed,
                            role = office,
                            link = generate_link(),
                            )
    return subordinate
def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance


def db_count(model):
    if db.session.query(model).first() is None: user = 1
    else: user = (db.session.query(model).order_by(model.id.desc()).first().id)
    return user

class Province(db.Model):
    __tablename__ = "provinces"
    id = db.Column(db.Integer, primary_key=True)    
    name = db.Column(db.String, nullable=False, unique=True)
    head  = db.relationship('Head', backref='province', uselist=False)
    employees  = db.relationship('StaffReturn', backref='province', lazy=True)
    departments  = db.relationship('Department', backref='province', lazy=True)

class Department(db.Model):
    __tablename__ = "departments"
    id = db.Column(db.Integer, primary_key=True)    
    name = db.Column(db.String, nullable=False)
    head  = db.relationship('Head', backref='department', uselist=False)
    districts  = db.relationship('District', backref='department', lazy=True)
    employees  = db.relationship('StaffReturn', backref='department', lazy=True)
    province_id = db.Column(db.Integer, db.ForeignKey('provinces.id'))

class District(db.Model):
    __tablename__ = "districts"
    id = db.Column(db.Integer, primary_key=True)    
    name = db.Column(db.String, nullable=False)
    head  = db.relationship('Head', backref='district', uselist=False)
    stations  = db.relationship('Station', backref='district', lazy=True)
    employees  = db.relationship('StaffReturn', backref='district', lazy=True)
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))

class Station(db.Model):
    __tablename__ = "stations"
    id = db.Column(db.Integer, primary_key=True)    
    name = db.Column(db.String, nullable=False, unique=True)
    distance_from_cbd = db.Column(db.Integer, nullable=False)
    head  = db.relationship('Head', backref='station', uselist=False)
    employees  = db.relationship('StaffReturn', backref='station', lazy=True)
    district_id = db.Column(db.Integer, db.ForeignKey('districts.id'))
 
class Head(db.Model):
    __tablename__ = "heads"

    # meta data
    id = db.Column(db.Integer, primary_key=True)    
    employee_number = db.Column(db.String, nullable=False, unique=True)
    months_acting = db.Column(db.Integer, nullable=False)    

    name = db.Column(db.String,  nullable=False)
    email_address = db.Column(db.String, nullable=False)
    role = db.Column(db.String, nullable=False)

    # FOREIGN KEYS
    province_id = db.Column(db.Integer, db.ForeignKey('provinces.id'))
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
    district_id = db.Column(db.Integer, db.ForeignKey('districts.id'))
    station_id = db.Column(db.Integer, db.ForeignKey('stations.id'))

    confirmed = db.Column(db.Integer, default=0)    
    active = db.Column(db.Integer, default=1)    

    link = db.Column(db.String, nullable=False, unique=True)
    
    # remove for station

class StaffReturn(db.Model):
    __tablename__ = "staff_returns"
    id = db.Column(db.Integer, primary_key=True)    
    status = db.Column(db.Integer, nullable=False, default=3)    
    months_acting = db.Column(db.Integer, nullable=False)    
    employee_number = db.Column(db.String, nullable=False, unique=True)
    
    name = db.Column(db.String,  nullable=False)
    email_address = db.Column(db.String, nullable=False)
    current_position = db.Column(db.String, nullable=False)

    province_id = db.Column(db.Integer, db.ForeignKey('provinces.id'))
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
    district_id = db.Column(db.Integer, db.ForeignKey('districts.id'))
    station_id = db.Column(db.Integer, db.ForeignKey('stations.id'))

    confirmed = db.Column(db.Integer, default=0)    
    active = db.Column(db.Integer, default=1)    

    # FAT
    role = db.Column(db.String, nullable=False)
    link = db.Column(db.String, nullable=False, unique=True)

