class BaseConfig(object):
    """Base configuration."""

    # main config
    SECRET_KEY = 'lusaka123'
    SECURITY_PASSWORD_SALT = 'lsk123456'
    DEBUG = False
    BCRYPT_LOG_ROUNDS = 13
    WTF_CSRF_ENABLED = True
    DEBUG_TB_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False

    # mail settings
    MAIL_SERVER = 'live-outlookmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True

    # outlook authentication
    MAIL_USERNAME = os.environ['APP_MAIL_USERNAME']
    MAIL_PASSWORD = os.environ['APP_MAIL_PASSWORD']

    # mail accounts
    MAIL_DEFAULT_SENDER = 'szi_dev@szi.gov.zm'

    SQlSERVER = "
    Server=localhost\MSSQLSERVER01
    ;Database=master;
    Trusted_Connection=True;"

    mssql+pyodbc://<username>:<password>@<dsnname>
