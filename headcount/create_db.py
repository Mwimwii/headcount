from flask import Flask
from models import *

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///app.sqlite"
# app.config["SQLALCHEMY_DATABASE_URI"] = "postgres://eexsdbprnkgsjz:a35cd06c81133b8d628935dbd239f2d531358cdf92953c8641a9a3efb9ef3fd9@ec2-52-2-6-71.compute-1.amazonaws.com:5432/d74bb49mlkr1uk"

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db.init_app(app)

def main():
    print("Dropping ALL tables")
    db.drop_all()
    print("Creating tables ")
    db.create_all()
    print("Successfully created model ")

if __name__ == "__main__":
    with app.app_context():
        main()
