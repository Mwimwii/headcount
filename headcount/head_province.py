from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
# from werkzeug.exceptions import abort

from headcount.models import *
from headcount.auth import generate_link
from headcount.forms import DistrictForm, TestForm, roles, provinces_districts
from headcount.admin import send_new_head_mail

bp = Blueprint('province', __name__, url_prefix='/province')

@bp.route('/')
def index():
    form = DistrictForm()
    form.district.choices = provinces_districts.get(g.user.department.province.name)
    sub_dep = list(e for e in g.user.department.districts  if e.head is not None)
    return render_template("head_province/home.html", sub_dep=sub_dep, form=form)


# ADD DISTRICT
@bp.route('/add', methods=["GET", "POST"])
def add_district_head():
    if g.user is None:
        return "Please Login"
    form = DistrictForm()
    form.district.choices = provinces_districts.get(g.user.department.province.name)

    if form.validate_on_submit():
        district = get_or_create(db.session, District, name=form.district.data, department_id=g.user.department.id)
        head = db.session.query(Head).filter(Head.employee_number == form.employee_number.data).first()
        if head is None and district.head is None:
            head = Head(name=form.name.data,
                                employee_number = form.employee_number.data,
                                email_address = form.email_address.data,
                                months_acting = 0,
                                role = roles.get(g.user.role),
                                link = generate_link(),
                                )
            district.head = head
            g.user.department.districts.append(district)
            db.session.add(head)
            db.session.commit()
            send_new_head_mail(head)            
        else:
            error = "The current employee number exists"
            flash(error)
    return redirect(url_for("admin.index"))

@bp.route('/<user>/edit', methods=["GET", "POST"])
def edit_district_head(user):
    head = db.session.query(Head).get(user)
    if (roles.get(g.user.role) != head.role):
        return "Not authorized"
    form = DistrictForm(obj=head)
    form.district.choices = provinces_districts.get(g.user.department.province.name)
    if form.validate_on_submit():
        
        error = is_valid_district(form, head)
        error = is_valid_employee_number(head, form)
        if error is None:
            head.name = form.name.data
            head.email_address  = form.email_address.data
            db.session.commit()
            return redirect(url_for("admin.index"))
        else:
            flash(error)
    form.district.data = head.district.name
    return render_template("head_province/form_edit.html", form=form,role=roles.get(g.user.role), user=user )

def is_valid_employee_number(head, form):
    error = None
    subordinate = db.session.query(Head).filter(Head.employee_number == form.employee_number.data).first()
    if subordinate is None:
        head.employee_number = form.employee_number.data
    elif head == subordinate:
        pass
    else:
        error = "User already exists"
    return error

def is_valid_district(form, head):
    error = None
    if "Select" in form.district.data:
        return error
    district = get_or_create(db.session, District, name=form.district.data, department_id=g.user.department.id)
    if district.head is None:
        district.head = head
    elif district.head == head:
        pass
    else:
        error = "The head already exists"
    return error


@bp.route("/staff-return", methods=["GET"])
def view_staff_return():
    staff_return = []
    for district in g.user.department.districts:
        staff_return.extend(district.employees)
        for station in district.stations:
            staff_return.extend(station.employees)
    staff_return = list(sf for sf in staff_return if sf.confirmed == 3)
    return render_template("staff__return/view_staff_return.html", employees=staff_return, my_staff_return=g.user.department.employees, hq_name=g.user.department.name)

@bp.route("/form")
def multi_form():
    form = TestForm
    print(request.data)
    return render_template('staff__return/add_staff_return.html', form=form)


@bp.route("/api", methods=["POST"])
def api():
    if request.method == "POST":
        message = []
        for employee in request.json: 
            if "''" not in list(employee.values()):
                subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == employee.get("employee_number")[1:-1]).first()
                if subordinate is None:
                    office = g.user.department.name + " - " + g.user.department.province.name + " Province HQ" 
                    subordinate = new_staff_return(employee, subordinate, 3, office )
                    g.user.department.employees.append(subordinate)
                    db.session.add(subordinate)
                    db.session.commit()
                    message.append({"id" : employee.get("id"), "msg":200 })
                else:
                    message.append({"id" : employee.get("id"), "msg":400 })
            else:
                message.append({"id" : employee.get("id"), "msg":400 })
        return jsonify(message)

@bp.route("/staff-return/submit", methods=["GET", "POST"])
def submit_staff_return():
    # station = db.session.query(Station).get(g.user.station_id)
    employees = g.user.department.employees
    # employees = list(e for e in station.employees)
    staff_return = []
    for district in g.user.department.districts:
        staff_return.extend(district.employees)
        for station in district.stations:
            staff_return.extend(station.employees)
    staff_return = list(sf for sf in staff_return if sf.confirmed == 2)

    return render_template("staff__return/submit_staff_return.html", all_employees=staff_return, employees=employees)

@bp.route("/submit", methods=["GET", "POST"])
def submit_collection():
    if request.method == "GET":
        return "This is the API"
    if request.method == "POST":
        message = []
        for employee in request.json: 
            subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == employee.get("employee_number")[1:-1]).first()
            if subordinate.confirmed == 2:
                subordinate.confirmed = 3 
            subordinate.status=int(employee.get("status")[1:-1])
            db.session.commit()
            message.append({employee.get("id"):200 })
        return jsonify(message)

