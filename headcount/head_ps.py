import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from headcount.models import StaffReturn, Department, Head, Province, db, get_or_create
from headcount.forms import PSForm, ProvinceForm, TestForm, roles, rt
from headcount.auth import generate_link
from headcount.admin import send_new_head_mail
bp = Blueprint('ps', __name__, url_prefix='/ps')

@bp.route('/')
def index():
    form = ProvinceForm()
    sub_dep = list(e for e in g.user.province.departments if e.head is not None)
    return render_template("head_ps/home.html", sub_dep=sub_dep, form=form)

@bp.route('/register', methods=["GET", "POST"])
def register():
    ## good
    form = PSForm()
    # If the form is valid
    if form.validate_on_submit():
        province = get_or_create(db.session, Province, name=form.province.data)
        if province.head is None:
                        # Create the new staff__return
            head = Head(name=form.name.data,
                        employee_number = form.employee_number.data,
                        email_address = form.email_address.data,
                        role = "Permanent Secretary",
                        months_acting = 0,
                        link = generate_link())
        
            province.head = head
            db.session.add(head)
            db.session.commit()
            return redirect(url_for("admin.send_one", user=head.id))
        else:
            error = "PS already exists for this Province"
            flash(error)
    return render_template("head_ps/form_registration.html", form=form)


# ADD PROVINCE
@bp.route('/add', methods=["GET", "POST"])
def add_provincial_head():
    # needs to change
    if g.user is None:
        return "Please Login"
    form = ProvinceForm()
    # If the form is valid
    if form.validate_on_submit():
        department = get_or_create(db.session, Department, name=form.department.data, province_id=g.user.province.id)
        head = db.session.query(Head).filter(Head.employee_number == form.employee_number.data).first()
        # Proceed to check if the user exists
        if head is None and department.head is None:
            # Create a new head
            head = Head(
                                    name=form.name.data,
                                    employee_number = form.employee_number.data,
                                    email_address = form.email_address.data,
                                    months_acting = 0,
                                    role = roles.get(g.user.role),
                                    link = generate_link(),
                                    )
            department.head = head
            g.user.province.departments.append(department)
            db.session.add(head)
            db.session.commit()
            send_new_head_mail(head)
        else:
            error = "Employee number exists or department head exists"
            flash(error)
    return redirect(url_for("admin.index"))

@bp.route('/<user>/edit', methods=["GET", "POST"])
def edit_provincial_head(user):
    head = db.session.query(Head).get(user)
    if (roles.get(g.user.role) != head.role):
        return "Not authorized"
    form = ProvinceForm(obj=head)
    if form.validate_on_submit():
        error = is_valid_department(form, head)
        error = is_valid_employee_number(head, form)
        if error is None:
            head.name = form.name.data
            head.email_address  = form.email_address.data
            db.session.commit()
            return redirect(url_for("admin.index"))
        else:
            flash(error)
    form.department.data = head.department.name
    return render_template("head_ps/form_edit.html", form=form,role=roles.get(g.user.role), user=user )

def is_valid_employee_number(head, form):
    error = None
    subordinate = db.session.query(Head).filter(Head.employee_number == form.employee_number.data).first()
    if subordinate is None:
        head.employee_number = form.employee_number.data
    elif head == subordinate:
        pass
    else:
        error = "User already exists"
    return error

def is_valid_department(form, head):
    error = None
    if "Select" in form.department.data:
        return error
    department = get_or_create(db.session, Department, name=form.department.data, province_id=g.user.province.id)
    if department.head is None:
        department.head = head
    elif department.head == head:
        pass
    else:
        error = "The head already exists"
    return error


@bp.route("/staff-return", methods=["GET"])
def view_staff_return():
    staff_return = []
    for department in g.user.province.departments:
        staff_return.extend(department.employees)
        for district in department.districts:
            staff_return.extend(district.employees)
            for station in district.stations:
                staff_return.extend(station.employees)
    staff_return = list(sf for sf in staff_return if sf.confirmed >= rt.get(g.user.role))
    # return render_template("staff_return_test/view_staff_return.html", employees=staff_return, my_staff_return=g.user.province.employees, hq_name=g.user.province.name)
    return render_template("staff__return/view_staff_return.html", employees=staff_return, my_staff_return=g.user.province.employees, hq_name=g.user.province.name)

@bp.route("/form")
def multi_form():
    form = TestForm
    # return render_template('staff_return_test/add_staff_return.html', form=form)
    return render_template('staff__return/add_staff_return.html', form=form)


@bp.route("/api", methods=["POST"])
def api():
    if request.method == "POST":
        message = []
        for employee in request.json: 
            if "''" not in list(employee.values()):
                subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == employee.get("employee_number")[1:-1]).first()
                if subordinate is None:
                    subordinate = StaffReturn(
                                            name=employee.get("name")[1:-1],
                                            employee_number=employee.get("employee_number")[1:-1],
                                            current_position=employee.get("current_position")[1:-1],
                                            email_address=employee.get("email_address")[1:-1],
                                            months_acting=employee.get("months_acting")[1:-1],
                                            status=employee.get("status")[1:-1],
                                            confirmed=4,
                                            role = roles.get(g.user.role),
                                            link = generate_link(),
                                            )
                    g.user.province.employees.append(subordinate)
                    db.session.add(subordinate)
                    db.session.commit()
                    message.append({"id" : employee.get("id"), "msg":200 })
                else:
                    message.append({"id" : employee.get("id"), "msg":400 })
            else:
                message.append({"id" : employee.get("id"), "msg":400 })
        return jsonify(message)


@bp.route("/staff-return/submit", methods=["GET", "POST"])
def submit_staff_return():
    # station = db.session.query(Station).get(g.user.station_id)
    employees = g.user.province.employees
    # employees = list(e for e in station.employees)
    staff_return = []
    for department in g.user.province.departments:
        staff_return.extend(department.employees)
        for district in department.districts:
            staff_return.extend(district.employees)
            print(list(e.confirmed for e in staff_return))
            print(list(e for e in district.employees))
            for station in district.stations:
                staff_return.extend(station.employees)
    staff_return = list(sf for sf in staff_return if sf.confirmed == 3)

    return render_template("staff__return/submit_staff_return.html", employees=employees, all_employees=staff_return)

@bp.route("/submit", methods=["GET", "POST"])
def submit_collection():
    if request.method == "GET":
        return "This is the API"
    if request.method == "POST":
        message = []
        for employee in request.json: 
            subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == employee.get("employee_number")[1:-1]).first()
            if subordinate.confirmed == 3:
                subordinate.confirmed = 4 
            subordinate.status=int(employee.get("status")[1:-1])
            db.session.commit()
            message.append({employee.get("id"):200 })
        return jsonify(message)

