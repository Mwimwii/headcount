from flask_wtf import FlaskForm
from wtforms import StringField,SelectField, SubmitField
from wtforms.validators import DataRequired, Length, Email

roles = {
    "Permanent Secretary":"Provincial",
    "Provincial":"District",
    "District":"Station",
    "Station":"Station Employee"
}
rt = {
    "Permanent Secretary":3,
    "Provincial":2,
    "District":1,
    "Station":0
}

provinces_districts = {'Lusaka': ['Chilanga', 'Chongwe', 'Rufunsa', 'Luangwa'], 'Central': ['Chibombo ', 'Kabwe', 'Kapiri Mposhi', 'Mkushi', 'Mumbwa', 'Serenje', 'Chisamba', 'Chitambo', 'Itezhi-Tezhi', 'Luano', 'Ngabwe', 'Shibuyunji'], 'Copperbelt': ['Chililabombwe', 'Chingola', 'Kalulushi', 'Kitwe', 'Luanshya', 'Lufwanyama', 'Mpongwe', 'Masaiti', 'Mufulira', 'Ndola'], 'Western ': ['Kalabo', 'Kaoma', 'Lukulu', 'Mongu', 'Senanga', 'Sesheke', 'Limulunga', 'Luampa', 'Mitete', 'Mulobezi', 'Mwandi', 'Nalolo', 'Nkeyema', "Shang'Ombo", 'Sikongo', 'Sioma'], 'North-Western': ['Mufumbwe', 'Kabompo', 'Kasempa', 'Mwinilunga', 'Solwezi', 'Zambezi', 'Chavuma', 'Ikelenge', 'Manyinga'], 'Eastern': ['Chadiza', 'Chama', 'Chipata', 'Katete', 'Lundazi', 'Nyimba', 'Petauke', 'Mambwe', 'Sinda', 'Vubwi'], 'Southern ': ['Choma', 'Mazabuka', 'Monze', 'Gwembe', 'Namwala', 'Livingstone', 'Kalomo', 'Siavonga', 'Sinazongwe', 'Chikananta', 'Kazungula', 'Pemba', 'Zimba'], 'Luapula': ['Kawambwa', 'Mansa', 'Mwense', 'Nchelenge', 'Samfya', 'Chembe', 'Chienge', 'Chipili', 'Lunga', 'Milenge', 'Mwansabombwe'], 'Northern ': ['Chilubi', 'Chinsali', 'Isoka', 'Kaputa', 'Kasama', 'Lubingu', 'Mbala', 'Mpika', 'Mporokoso', 'Nakonde', 'Luwingu', 'Mpulungu', 'Mungwi', 'Nsama'], 'Muchinga': 
['Mafinga', "Shiwang'Andu"]}

districts = ['Chilanga', 'Chongwe', 'Rufunsa', 'Kafue', 'Chirundu', 'Luangwa', 'Chibombo ', 'Kabwe', 'Kapiri Mposhi', 'Mkushi', 'Mumbwa', 'Serenje', 'Chililabombwe', 'Chingola', 'Kalulushi', 'Kitwe', 'Luanshya', 'Lufwanyama', 'Mpongwe', 'Masaiti', 'Mufulira', 'Ndola', 'Chadiza', 'Chama', 'Chipata', 'Katete', 'Lundazi', 'Nyimba', 'Petauke', 'Kawambwa', 'Mansa', 'Mwense', 'Nchelenge', 'Samfya', 'Chilubi', 'Chinsali', 'Isoka', 'Kaputa', 'Kasama', 'Lubingu', 'Mbala', 'Mpika', 'Mporokoso', 'Nakonde', 'Mufumbwe', 'Kabompo', 'Kasempa', 'Mwinilunga', 'Solwezi', 'Zambezi', 'Choma', 'Mazabuka', 'Monze', 'Gwembe', 'Namwala', 'Livingstone', 'Kalomo', 'Siavonga', 'Sinazongwe', 'Kalabo', 'Kaoma', 'Lukulu', 'Mongu', 'Senanga', 'Sesheke', 'Chisamba', 'Chitambo', 'Itezhi-Tezhi', 'Luano', 'Ngabwe', 'Shibuyunji', 'Mambwe', 'Sinda', 'Vubwi', 'Chembe', 'Chienge', 'Chipili', 'Lunga', 'Milenge', 'Mwansabombwe', 'Mafinga', "Shiwang'Andu", 'Luwingu', 'Mpulungu', 'Mungwi', 'Nsama', 'Chavuma', 'Ikelenge', 'Manyinga', 'Chikananta', 'Kazungula', 'Pemba', 'Zimba', 'Limulunga', 'Luampa', 'Mitete', 'Mulobezi', 'Mwandi', 'Nalolo', 'Nkeyema']
departments = ['Select Department','Public Service Management Division (PSMD)', 'Office of the Auditor General', 'Ministry of Defence', 'Ministry of Home Affairs', 'Ministry of Foreign Affairs', 'Ministry of Finance', 'Ministry of Justice', 'Ministry of National Development Planning', 'Ministry of Agriculture', 'Ministry of Fisheries and Livestock', 'Ministry of Commerce, Trade and Industry', 'Ministry of Mines and Minerals Development', 'Ministry of Energy', 'Ministry of Tourism and Arts', 'Ministry of Transport and Communication', 'Ministry of Works and Supply', 'Ministry of Housing and Infrastructure Development', 'Ministry of Lands and Natural Resources', 'Ministry of Water Development, Sanitation and Environmental Protection', 'Ministry of Health', 'Ministry of Information and Broadcasting Services', 'Ministry of General Education', 'Ministry of Higher Education', 'Ministry of Local Government', 'Ministry of Labour and Social Security', 'Ministry of Community Development and Social Welfare', 'Ministry of Gender', 'Ministry of Youth, Sport and Child Development', 'Ministry of Chiefs and Traditional Affairs', 'Ministry of National Guidance and Religious Affairs']
provinces = ['Select Province', 'Lusaka', 'Central', 'Copperbelt', 'Western ', 'North-Western', 'Eastern', 'Southern ', 'Luapula', 'Northern ', 'Muchinga']

def is_valid_employee_number(db, head, form):
    error = None
    subordinate = db.session.query(Head).filter(Head.employee_number == form.employee_number.data).first()
    if subordinate is None:
        head.employee_number = form.employee_number.data
    elif head == subordinate:
        pass
    else:
        error = "User already exists"
    return error



class TestForm(FlaskForm):
    name = StringField(
        'Name',
        [DataRequired()]
    )

class LoginForm(FlaskForm):
     email = StringField(
        'Email',[Email(message=('Not a valid email address.')),],
    )

class EmployeeForm(FlaskForm):
    name = StringField(
        'Name',
        [DataRequired()]
    )
    employee_number = StringField(
        'Employee Number',
        [DataRequired()]
    )
    email_address = StringField(
        'Email',
        [Email(message=('Not a valid email address.')),]
    )
    submit = SubmitField('Submit')

class StaffReturnForm(EmployeeForm):
    current_position = StringField(
        'Current Position')
    months_acting = StringField(
        'Months Acting')

class PSForm(EmployeeForm):
    province = SelectField('Province',choices=provinces)

class ProvinceForm(EmployeeForm):
    department = SelectField('Department',choices=departments,
    default="N/A")
    
class DistrictForm(EmployeeForm):
    district = SelectField('District')

class StationForm(EmployeeForm):
    station = StringField(
        'Station',
        [DataRequired()]
    )
    distance_from_cbd = StringField(
        'Distance from CBD'
    )