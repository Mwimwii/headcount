from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from headcount.models import * 
from headcount.auth import generate_link
from headcount.forms import EmployeeForm, StationForm, roles
from headcount.admin import send_new_head_mail

bp = Blueprint('staff_return', __name__, url_prefix='/staff-return')



# SECURE THE API
# GET THE ROLE OF THE USER
# DEPENDING ON THE ROLE, THAT WILL BE THE LEVEL OF VERIFICATION OF STAFF RETURNS

@bp.route("/submit", methods=["GET", "POST"])
def submit_collection():
    if request.method == "GET":
        return "This is the API"
    if request.method == "POST":
        message = []
        for employee in request.json: 
            subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == employee.get("employee_number")[1:-1]).first()
            subordinate.confirmed = 1
            subordinate.status=employee.get("status")[1:-1],
            db.session.commit()
            message.append({employee.get("id"):200 })
        return jsonify(message)


@bp.route("/staff_return/api", methods=["GET", "POST"])
def staff_return_submit_api():
    if request.method == "GET":
        return "This is the API"
    if request.method == "POST":
        message = []
        for employee in request.json: 
            print("Working  1")
            subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == employee.get("employee_number")[1:-1]).first()
            print("Working  2")
            subordinate.confirmed = True
            subordinate.status=employee.get("status")[1:-1],
            db.session.commit()
            message.append({employee.get("id"):200 })
        return jsonify(message)
