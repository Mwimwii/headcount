from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from flask.helpers import make_response
from werkzeug.exceptions import abort

from headcount.models import * 
from headcount.forms import StaffReturnForm, TestForm, roles
from headcount.auth import generate_link


bp = Blueprint('station', __name__, url_prefix='/station')

@bp.route('/')
def index():
    if g.user is None:
        return "Please Login"
    form = StaffReturnForm()
    subordinates = g.user.station.employees
    return render_template("head_station/home.html", sub_dep=subordinates, form = form, role=roles.get(g.user.role))


# ADD Station Employee
@bp.route('/add', methods=["GET", "POST"])
def add():
    if g.user is None:
        return "Please Login"
    form = StaffReturnForm()
    if form.validate_on_submit():
        subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == form.employee_number.data).first()
        if subordinate is None:
            subordinate = StaffReturn(name=form.name.data,
                                    employee_number = form.employee_number.data,
                                    current_position = form.current_position.data,
                                    email_address = form.email_address.data,
                                    months_acting = 0,
                                    role = roles.get(g.user.role),
                                    link = generate_link(),
                                    )
            g.user.station.employees.append(subordinate)
            db.session.add(subordinate)
            db.session.commit()
        else:
            error = "The current employee number exists"
            flash(error)
    return redirect(url_for("admin.index"))

@bp.route('/<user>/edit', methods=["GET", "POST"])
def edit(user):
    employee = db.session.query(StaffReturn).get(user)
    role = employee.role
    if (roles.get(g.user.role) != employee.role) and (g.user.id != employee.id):
        return "Not authorized"
    form = StaffReturnForm(obj=employee)
    if form.validate_on_submit():
        subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == form.employee_number.data).first()
        employee.name = form.name.data
        employee.email_address  = form.email_address.data
        if subordinate is None:
            employee.employee_number = form.employee_number.data
        elif subordinate.name == employee.name:
            error = "The current employee number exists"
            flash(error)
        db.session.commit()
        return redirect(url_for("station.index"))
    return render_template("head_station/form_edit.html", form=form,role=role, user=user )

@bp.route("/add/multi", methods = ["POST"])
def post_many():
    print(request.data)
    return "200"


@bp.route("/form")
def multi_form():
    form = TestForm
    print(request.data)
    return render_template('staff__return/add_staff_return.html', form=form)


@bp.route("/api", methods=["POST"])
def api():
    if request.method == "POST":
        message = []
        for employee in request.json: 
            if "''" not in list(employee.values()):
                subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == employee.get("employee_number")[1:-1]).first()
                if subordinate is None:
                    office = g.user.station.name + " - " + g.user.station.district.name +" "+  g.user.station.district.department.name+ " "+g.user.station.district.department.province.name 
                    subordinate = new_staff_return(employee, subordinate, 1, office )
                    g.user.station.employees.append(subordinate)
                    db.session.add(subordinate)
                    db.session.commit()
                    message.append({"id" : employee.get("id"), "msg":200 })
                else:
                    message.append({"id" : employee.get("id"), "msg":400 })
            else:
                message.append({"id" : employee.get("id"), "msg":400 })
        return jsonify(message)


@bp.route("/staff-return", methods=["GET"])
def view_staff_return():
    station = db.session.query(Station).get(g.user.station_id)
    
    return render_template("staff__return/view_staff_return.html", employees=station.employees)


@bp.route("/staff-return/submit", methods=["GET", "POST"])
def submit_staff_return():
    station = db.session.query(Station).get(g.user.station_id)
    # employees = list(e for e in station.employees if not e.confirmed)
    employees = list(e for e in station.employees)
    print(employees)
    return render_template("staff__return/submit_staff_return.html", employees=employees)

@bp.route("/submit", methods=["GET", "POST"])
def submit_collection():
    if request.method == "GET":
        return "This is the API"
    if request.method == "POST":
        message = []
        for employee in request.json: 
            subordinate = db.session.query(StaffReturn).filter(StaffReturn.employee_number == employee.get("employee_number")[1:-1]).first()
            if subordinate.confirmed == 0:
                subordinate.confirmed = 1
            subordinate.status=int(employee.get("status")[1:-1])
            db.session.commit()
            message.append({employee.get("id"):200 })
        return jsonify(message)
