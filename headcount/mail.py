import requests

def send_email(email_recipient,
               email_subject,
               email_message):
    # url = 'http://127.0.0.1:5000/'
    url = 'https://lit-forest-23594.herokuapp.com/'
    myobj = {'to': email_recipient,
            'subject': email_subject,
            'body': email_message
            }
    x = requests.post(url, json = myobj)