from .models import *
import os
from flask import Flask

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='af08igu9ing',
        SQLALCHEMY_DATABASE_URI = 'sqlite:///app.sqlite',
        # SQLALCHEMY_DATABASE_URI = "postgres://eexsdbprnkgsjz:a35cd06c81133b8d628935dbd239f2d531358cdf92953c8641a9a3efb9ef3fd9@ec2-52-2-6-71.compute-1.amazonaws.com:5432/d74bb49mlkr1uk",
        SQLALCHEMY_TRACK_MODIFICATIONS = False
    )
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    db.init_app(app)
    from flask_wtf.csrf import CSRFProtect
    CSRFProtect(app) 
    from . import auth
    app.register_blueprint(auth.bp)


    from . import admin
    app.register_blueprint(admin.bp)
    app.add_url_rule("/", endpoint="index")

    from . import head_ps
    app.register_blueprint(head_ps.bp)
    
    from . import head_province
    app.register_blueprint(head_province.bp)

    from . import head_district
    app.register_blueprint(head_district.bp)

    from . import head_station
    app.register_blueprint(head_station.bp)

    from . import staff_return
    app.register_blueprint(staff_return.bp)


    return app