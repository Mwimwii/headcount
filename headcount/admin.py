from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort

from headcount.models import * 
from headcount.forms import ProvinceForm, roles
from headcount.auth import generate_link, login_required
from .mail import send_email
bp = Blueprint('admin', __name__)


def send_new_head_mail(head):
    link = url_for("auth.login", employee_id=head.id, credentials=head.link, _external=True)
    html = render_template("email/confirm_message.html", fileAtt='images/h1.gif', link=link)
    send_email(head.email_address, "Headcount link confirmation", html)

# main view that displays a table of subordinates
@bp.route('/')
@login_required
# @login_required -> fix
def index():
    if g.user is None:
        return "Please Login"
    role = roles.get(g.user.role, "Employee")
    if role == 'Provincial':
        return redirect(url_for("ps.index"))
    elif role == 'District':
        return redirect(url_for("province.index"))
    elif role == 'Station':
        return redirect(url_for("district.index"))
    else:
        return redirect(url_for("station.index"))
    
# Refresh the links
@bp.route('/api/refresh')
def refresh():
    employees = db.session.query(Head).all()
    newhashes = []
    for e in employees:
        row = {}
        row["Old"]=e.link
        e.link = generate_link()
        row["New"]=e.link
        newhashes.append(row)
    db.session.commit()
    return jsonify(newhashes)

@bp.route('/api/all')
def send():
    employees = db.session.query(Head).all()
    tb = []
    tb.append({"Register":"http://10.2.71.234:5000/auth/register"})

    for employee in employees:
        link = url_for("auth.login",employee_id=employee.id, credentials=employee.link, _external=True)
        # html = render_template("email/confirm.html", link=link)
        employee_has_email = employee.email_address
        message_satus = "User does not have an email"
        if employee_has_email:
            # send_email(staff__return.email_address, "Headcount link confirmation", html)
            message_satus = "Email confirmation has been sent"
        # tb.append({"link":link, "email":staff__return.email_address, "Email":message_satus})
        tb.append({"link":link, "email":employee.email_address})
    return jsonify(tb)

@bp.route('/api/<user>/send')
def api_send_one(user):
    employee = db.session.query(Head).get(user)

    link = url_for("auth.login",employee_id=employee.id, credentials=employee.link, _external=True)
    html = render_template("email/confirm_message.html", fileAtt='images/h1.gif', link=link)
    send_email(employee.email_address, "Headcount link confirmation", html)
    return jsonify({"link":link, "email":employee.email_address, "Email":"Email_sent"})

@bp.route('/email/<user>/send')
def send_one(user):
    employee = db.session.query(Head).get(user)
    link = url_for("auth.login",employee_id=employee.id, credentials=employee.link, _external=True)
    html = render_template("email/confirm_message.html", fileAtt='images/h1.gif', link=link)
    send_email(employee.email_address, "Headcount link confirmation", html)
    return render_template('head_ps/registration_success.html', email=employee.email_address)
# 
@bp.route('/<user>/edit', methods=["GET", "POST"])
def edit(user):
    employee = db.session.query(Head).get(user)
    role = employee.role

    # If the current user not the direct head AND
    # If the current user is not g.user the throw 
    # Throw 403. 
    if (g.user.id != employee.created_by) and (g.user.id != employee.id):
        return "Not authorized"
    form = ProvinceForm(obj=employee)
    if form.validate_on_submit():
        employee.name = form.name.data
        employee.email_address  = form.email_address.data
        employee.povince  = form.province.data
        subordinate = db.session.query(Head).filter(Head.employee_number == form.employee_number.data).first()
        if subordinate is None:
            employee.employee_number = form.employee_number.data
        elif subordinate.name == employee.name:
            error = "The current staff__return number exists"
            flash(error)
        db.session.commit()
        return redirect(url_for("admin.index"))
    return render_template("admin/edit.html", form=form,role=role, user=user )

@bp.route('/<user>/remove', methods=["GET", "POST"])
def remove(user):
    if g.user is None:
        return "Please Login"
        
    if roles.get(g.user.role) == "Station Employee":
        employee = db.session.query(StaffReturn).get(user)
    else:
        employee = db.session.query(Head).get(user)
    if (roles.get(g.user.role) != employee.role):
        return "Not authorized"

    db.session.delete(employee)
    db.session.commit()
    return redirect(url_for('admin.index'))
    

