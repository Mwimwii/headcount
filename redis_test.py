from flask import Flask


def create_app(testConfig = None):
    app = Flask(__name__)
    app.register_blueprint(routes.main_bp)
    return app